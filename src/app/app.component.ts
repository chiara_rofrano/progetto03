import { Component } from '@angular/core';
import { Post } from './_model/post';
import { PostService } from './_service/post-service';
import { from, of } from 'rxjs';
import { filter, switchMap, concatMap, finalize } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { trigger, state, style, transition, animate } from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('ng-hide-remote', [
      state('state1', style({
        height: '0'
      })),
      state('state2', style({
        height: '1000px'
      })),
      transition('state1=>state2', animate('10000ms')),
      transition('state2=>state1', animate('1000ms'))
    ])
  ]
})
export class AppComponent {
  title = 'progetto6';
  current = 'state1'
  postList: Post[];
  Comment: string;
  commentList: Comment[];

  showComments() {
    this.getLastCommentsFromPostIdPari();

  }

  constructor(private postService: PostService, private http: HttpClient) {
    this.commentList = [];
  }

  ngOnInit() {
    //  this.postService.getAllPost().subscribe(
    //    response => {
    //      this.postList = (<Post[]> response);
    //    },
    //    error => {
    //      console.log (error.massages);
    //    }
    //  );
    //  this.getPostTitlesAndBodyOfEventIdPost(); 
    //  this.getLastCommentsFromPostIdPari();
  }

  getPostTitlesAndBodyOfEventIdPost() {
    this.http.get('https://jsonplaceholder.typicode.com/posts')
      .pipe(
        switchMap((postList: any) => from(postList)),
        filter((postList: any) => (postList.id % 2 == 0))
      )
      .subscribe((postList: any) => {
        console.log(postList.title)
        console.log(postList.body)
      })

  }

  getLastCommentsFromPostIdPari() {
    this.http.get('https://jsonplaceholder.typicode.com/posts')
      .pipe(
        //diventa observable di post=> cancella chiamata precedente
        switchMap((postList: any) => from(postList)),
        // prende post pari
        filter((singlePostList: any) => (singlePostList.id % 2 == 0)),
        concatMap((evenPost: any) => this.http.get
          ('https://jsonplaceholder.typicode.com/comments?postId=' + evenPost.id)),
        switchMap((comments: any) => of(comments[comments.length - 1])),
        finalize(() => this.current = 'state2')
      )

      .subscribe((lastComment: any) => this.commentList.push(<Comment>lastComment))
  }


}
